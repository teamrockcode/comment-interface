<?php

namespace TeamRock\Interfaces\Comment;

/**
 * Interface Commentable
 * @package TeamRock\Interfaces\Comment;
 */
interface Commentable
{
    public function getIdentifier();
}
