<?php

namespace TeamRock\Interfaces\Comment;

/**
 * Interface Comment
 * @package TeamRock\Interfaces\Comment
 */
interface Comment
{
    const STATE_PUBLISHED = 0;
    const STATE_REPORTED = 1;
    const STATE_DELETED = 2;

    /**
     * @return string
     */
    public function getIdentifier();

    /**
     * @return Commentable
     */
    public function getObject();

    /**
     * @return Comment|null
     */
    public function getParent();

    /**
     * @return string
     */
    public function getComment();

    /**
     * @return string
     * @todo MemberInterface
     */
    public function getAuthor();

    /**
     * @return int
     */
    public function getState();
}
