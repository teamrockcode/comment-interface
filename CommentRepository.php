<?php

namespace TeamRock\Interfaces\Comment;

interface CommentRepository
{
    /**
     * @param Commentable $object Find comments relating to $object
     * @return array
     */
    public function findComments($object);

    /**
     * @param Comment $comment Find replies to $comment
     * @return array
     */
    public function findReplies($comment);

    /**
     * Save a comment
     * @param Comment $comment A comment to save to the database
     */
    public function save($comment);

    /**
     * Flush the write buffer
     */
    public function flush();
}
